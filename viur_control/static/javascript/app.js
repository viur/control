$(document).ready(function () {
    var hostname = location.hostname;
    var myhref = $("a.open")[0];
    if (myhref) {
        myhref.href = myhref.href.replace("localhost", hostname);
    }
    myhref = $("a.startvi")[0];
    if (myhref) {
        myhref.href = myhref.href.replace("localhost", hostname);
    }

    myhref = $("a.openadmin")[0];
    if (myhref) {
        myhref.href = myhref.href.replace("localhost", hostname);
    }

    $(".serverlog").click(function() {
        $(this).addClass("active")
        window.open("/serverlog", "_blank");
    });

    $('#save-settings').click(function () {
        console.log("saving");
        $('#settings-form').submit();
    });

    $('.ctl-server-toggle').click(function() {
        var self = $(this);
        var digest = self.data("digest");
        var url;
        var activate = false;
        if ($(this).hasClass("active")) {
            url = "/stop_project";
        }
        else {
            url = "/start_project/" + digest;
            activate = true;
        }
        $.get(url, {"async": true}, function(data) {
            console.log("data", data);
            self.removeClass("active");
            var myctl = $("#ctl-current-" + digest);
            $(".serverlog").removeClass("active");

            $(".ctl-current").each(function(ix, value) {
                $(value).removeClass("active");
            });
            if (activate) {
                self.addClass("active");
                self.text("Stop");
                myctl.addClass("active");
            }
        });
    });
});
