#!/usr/bin/python
# -*- coding: utf-8 -*-

import ConfigParser
import datetime
import hashlib
import json
import logging
import os
import os.path
import re
import shlex
import signal
import sys

import gevent
import yaml
from blinker import Namespace
from flask import Flask, redirect, Response, request
from flask import render_template
from gevent import subprocess
from gevent.wsgi import WSGIServer

gevent.monkey.patch_all()

my_signals = Namespace()
stream_closed = my_signals.signal('stream-closed')

app = Flask(__name__)
app.viur_current_build = None
app.viur_current_deploy = None
app.viur_current_server = None
app.viur_projects = list()
app.viur_project_map = dict()

app.logger.setLevel(logging.DEBUG)
app.debug = True

NETWORKING_SETTINGS = [
	["FRONTEND_SERVER_PORT", 8080],
	["FRONTEND_ADMIN_PORT", 8000],
	["REAL_SERVER_PORT", 8080],
	["REAL_ADMIN_PORT", 8000],
]

STREAM_SETTINGS = [
	"LOG_CLOSE_ON_END_BUILD_VI",
	"LOG_CLOSE_ON_END_PROJECT",
	"LOG_CLOSE_ON_END_DEPLOY"
]

DIRECTORY_SETTINGS = [
	["VIUR_PROJECT_DIRECTORY", "/Projects"],
	["GOOGLE_SDK_PATH", "/src/google_appengine"]
]


def read_stream(project_name, proc_name, out_or_err=True, cleanup=False, close_window=False):
	"""

	:param project_name:
	:type project_name: str

	:param proc_name:
	:type proc_name: str
	:param out_or_err:
	:type out_or_err: bool
	:param cleanup:
	:type cleanup: bool
	"""

	def inner():
		m = re.compile(".* Username: (.*), Password: (.*)")
		log_handle = "{0}_{1}".format(proc_name, "log")
		project = getattr(app, project_name)
		if not project:
			return
		try:
			proc = project[proc_name]
			proc_pipe = out_or_err and proc.stdout.readline or proc.stderr.readline

			while True:
				if log_handle not in project:
					return
				data = proc_pipe()

				if data == "":
					break
				else:
					tmp = unicode(data.decode("utf-8")).replace(
							u"DEBUG", u'<span class="debug">DEBUG</span>').replace(
							u"INFO", u'<span class="info">INFO</span>').replace(
							u"ERROR", u'<span class="error">ERROR</span>').replace(
							u"WARNING", u'<span class="warning">WARNING</span>')
					matched = m.match(tmp)
					if matched:
						project["username"], project["password"] = matched.groups()
						write_password_file(project)
					yield u"data: {0}\n\n".format(tmp)

			app.logger.debug("read stream broken")
			if close_window:
				try:
					del project[log_handle]
				except:
					pass
				yield u"data: ---closestream---\n\n"
			gevent.sleep(0.001)
			if cleanup:
				cleanup_project(project_name, proc_name, project, proc)

		except KeyError:
			pass
		except Exception, err:
			app.logger.exception(err)
			pass

	return Response(inner(), mimetype="text/event-stream")


def cleanup_proc(project, proc_name, proc=None):
	if not proc:
		proc = project.get(proc_name)
		if not proc:
			return False

	pid = proc.pid

	try:
		proc.terminate()
	except Exception, err:
		pass
	try:
		proc.wait()
	except Exception, err:
		pass

	try:
		os.kill(pid, signal.SIGKILL)
	except:
		pass

	try:
		del project[proc_name]
	except Exception, err:
		pass

	return True


def cleanup_project(project_name, proc_name, project=None, proc=None):
	"""Tries very hard to clean up every attribute and instance of our subprocess

	:param project_name: the project dict in which we want to delete a subprocess
	:param proc_name:
	:param project:
	:param proc:
	:return:
	"""

	if not project:
		project = getattr(app, project_name)
		if not project:
			return

	if cleanup_proc(project, proc_name, proc):
		try:
			setattr(app, project_name, None)
		except:
			pass


def cleanup_projects():
	mappings = {
		"server_proc": "viur_current_server",
		"build_proc": "viur_current_build",
		"deploy_proc": "viur_current_deploy"
	}

	while app.viur_projects:
		project = app.viur_projects.pop()
		for proc_name, project_name in mappings.iteritems():
			try:
				cleanup_project(project_name, proc_name, project)
			except Exception, err:
				app.logger.exception(err)
				pass

	app.viur_project_map.clear()


def _reset():
	try:
		projectDirectory = app.config["VIUR_PROJECT_DIRECTORY"]
		app.logger.debug("using project directory: %r", projectDirectory)
		cleanup_projects()
		app.viur_project_map.clear()
		for root, directories, files in os.walk(projectDirectory):
			for directory in directories:
				path = os.path.join(root, directory)
				server_path = os.path.join(path, "server", "bones")
				if not os.path.isdir(server_path):
					continue

				try:
					appYaml = open(os.path.join(root, directory, "app.yaml"))
					config = yaml.load(appYaml)
					sha256 = hashlib.sha256()
					sha256.update(path)
					digest = sha256.hexdigest()
					tmp = {
						"path": path,
						"relpath": os.path.relpath(path, projectDirectory),
						"name": config["application"],
						"version": config["version"],
						"digest": digest
					}
					read_password_file(tmp)
					app.viur_projects.append(tmp)
					app.viur_project_map[digest] = tmp
				except Exception, err:
					pass
		app.viur_projects.sort(key=lambda x: x["name"])
	except Exception, err:
		app.logger.exception(err)
		pass


# starting processes
@app.route('/build_vi/<digest>')
def build_vi(digest):
	try:
		project = app.viur_project_map[digest]
		cmd_tpl = "/usr/bin/make deploy"
		path = os.path.abspath(os.path.join(project["path"], "..", "vi"))
		project["build_proc"] = subprocess.Popen(shlex.split(cmd_tpl), shell=False, stdout=subprocess.PIPE, cwd=path)
		app.viur_current_build = project
	except Exception, err:
		app.logger.exception(err)

	return redirect("/buildlog", code=302)


@app.route('/deploy_project/<digest>')
def deploy_project(digest):
	try:
		project = app.viur_project_map[digest]
		cmd_tpl = "/home/stefan/dev/google_appengine/appcfg.py update ."
		project["deploy_proc"] = subprocess.Popen(
				shlex.split(cmd_tpl),
				shell=False,
				cwd=project["path"],
				stderr=subprocess.PIPE)
		app.viur_current_deploy = project
	except:
		pass

	return redirect("/deploylog", code=302)


@app.route("/start_project/<digest>")
def start_project(digest):
	async = request.args.get("async")
	app.logger.debug("start_project: %r, %r", digest, async)
	cleanup_project("viur_current_server", "server_proc")
	success = False
	try:
		project = app.viur_project_map[digest]
		print "projectDir", project["path"]
		cmd_tpl = "{dev_appserver} --admin_host=0.0.0.0 --admin_port={admin_port} --host=0.0.0.0 --port={server_port} --skip_sdk_update_check=true --automatic_restart=true --storage_path {projectDir}/../storage/ --log_level debug {projectDir}".format(
				dev_appserver=os.path.join(app.config["GOOGLE_SDK_PATH"], "dev_appserver.py"),
				projectDir=project["path"],
				server_port=app.config["REAL_SERVER_PORT"],
				admin_port=app.config["REAL_ADMIN_PORT"]
		)
		project["server_proc"] = subprocess.Popen(
				shlex.split(cmd_tpl),
				shell=False,
				stderr=subprocess.PIPE)
		app.viur_current_server = project
		success = True
	except Exception, err:
		app.logger.exception(err)

	if not async:
		return redirect("/", code=302)
	else:
		return Response(
				json.dumps({"status": success, "digest": digest}), status=200,
				mimetype="application/json")


@app.route('/stop_project')
def stop_project():
	async = request.args.get("async")
	cleanup_project("viur_current_server", "server_proc")
	if not async:
		return redirect("/", code=302)
	else:
		return Response(
				json.dumps({"status": True}), status=200,
				mimetype="application/json")


# server side event endpoints
@app.route('/buildstream')
def buildstream():
	return read_stream(
			"viur_current_build", "build_proc", cleanup=True,
			close_window=app.config.get("LOG_CLOSE_ON_END_BUILD_VI", False))


@app.route('/deploystream')
def deploystream():
	return read_stream(
			"viur_current_deploy", "deploy_proc", False, cleanup=True,
			close_window=app.config.get("LOG_CLOSE_ON_END_DEPLOY", False))


@app.route('/serverstream')
def serverstream():
	return read_stream(
			"viur_current_server", "server_proc", False, cleanup=True,
			close_window=app.config.get("LOG_CLOSE_ON_END_PROJECT", False))


# http views
@app.route("/buildlog")
def buildlog():
	log_handle = "build_proc_log"
	if app.viur_current_build and app.viur_current_build.get(log_handle):
		return redirect("/", code=302)
	app.viur_current_build[log_handle] = True
	return render_template('output.jinja', target_stream="buildstream", digest=app.viur_current_build["digest"],
	                       log_title="Buildlog", log_handle=log_handle)


@app.route("/deploylog")
def deploylog():
	log_handle = "deploy_proc_log"
	if app.viur_current_deploy and app.viur_current_deploy.get(log_handle):
		return redirect("/", code=302)
	app.viur_current_deploy[log_handle] = True
	return render_template('output.jinja', target_stream="deploystream", digest=app.viur_current_deploy["digest"],
	                       log_title="Deploylog", log_handle=log_handle)


@app.route("/serverlog")
def serverlog():
	log_handle = "server_proc_log"
	if app.viur_current_server and app.viur_current_server.get(log_handle):
		return redirect("/", code=302)
	app.viur_current_server[log_handle] = True
	return render_template('output.jinja', target_stream="serverstream", log_title="Serverlog",
	                       digest=app.viur_current_server["digest"], log_handle=log_handle,
	                       close_window=app.config.get("LOG_CLOSE_ON_END_PROJECT", False))


@app.route("/closestream/<digest>/<log_handle>")
def close_stream(digest, log_handle):
	try:
		project = app.viur_project_map[digest]
		del project[log_handle]
		stream_closed.send(digest, log_handle)
	except KeyError:
		pass
	except Exception, err:
		app.logger.exception(err)
	return Response("{status:'ok'}", status=200, mimetype="application/json")


@app.route('/status')
def status():
	if not app.viur_projects or not app.viur_project_map:
		_reset()

	class Encoder(json.JSONEncoder):
		def default(self, obj):
			if isinstance(obj, datetime.datetime):
				return obj.strftime("%Y-%m-%d %H:%M")
			elif isinstance(obj, datetime.date):
				return obj.strftime("%Y-%m-%d")
			elif isinstance(obj, datetime.time):
				return obj.strftime("%H:%M")
			elif isinstance(obj, subprocess.Popen):
				return repr(obj)
			return json.JSONEncoder.default(self, obj)

	return Response(json.dumps({"projects": app.viur_projects}, cls=Encoder, indent=4, sort_keys=True), status=200,
	                mimetype="application/json")


@app.route('/')
def index():
	if not app.viur_projects or not app.viur_project_map:
		_reset()

	return render_template(
			'projects.jinja',
			projects=app.viur_projects,
			currentProject=app.viur_current_server,
			server_port=app.config.get("FRONTEND_SERVER_PORT", 8080),
			admin_port=app.config.get("FRONTEND_ADMIN_PORT", 8000))


@app.route('/reset')
def reset():
	_reset()
	return redirect("/", code=302)


@app.route("/settings", methods=['GET', 'POST'])
def settings():
	if request.method == "POST":
		pd = request.form["projects_directory"]
		old_pd = app.config["VIUR_PROJECT_DIRECTORY"]
		app.config["VIUR_PROJECT_DIRECTORY"] = pd
		if pd != old_pd:
			_reset()
		app.config["GOOGLE_SDK_PATH"] = request.form["google_sdk_path"]
		app.config["FRONTEND_SERVER_PORT"] = request.form["frontend_server_port"]
		app.config["FRONTEND_ADMIN_PORT"] = request.form["frontend_admin_port"]
		app.config["REAL_SERVER_PORT"] = request.form["real_server_port"]
		app.config["REAL_ADMIN_PORT"] = request.form["real_admin_port"]
		app.config["LOG_CLOSE_ON_END_PROJECT"] = "close_server" in request.form and request.form.get(
				"close_server") == "1"
		app.config["LOG_CLOSE_ON_END_BUILD_VI"] = "close_build" in request.form and request.form.get(
				"close_build") == "1"
		app.config["LOG_CLOSE_ON_END_DEPLOY"] = "close_deploy" in request.form and request.form.get(
				"close_deploy") == "1"
		save_frontend_settings()
		return redirect("/settings")
	return render_template(
			'settings.jinja',
			projects_directory=app.config.get("VIUR_PROJECT_DIRECTORY"),
			google_sdk_path=app.config.get("GOOGLE_SDK_PATH"),
			real_server_port=app.config.get("REAL_SERVER_PORT", 8080),
			real_admin_port=app.config.get("REAL_ADMIN_PORT", 8000),
			frontend_server_port=app.config.get("FRONTEND_SERVER_PORT", 8080),
			frontend_admin_port=app.config.get("FRONTEND_ADMIN_PORT", 8000),
			close_server=app.config.get("LOG_CLOSE_ON_END_PROJECT", False),
			close_build=app.config.get("LOG_CLOSE_ON_END_BUILD_VI", False),
			close_deploy=app.config.get("LOG_CLOSE_ON_END_DEPLOY", False)
	)


def write_password_file(project):
	config_parser = ConfigParser.ConfigParser()
	path = os.path.abspath(os.path.join(project["path"], "..", "pw"))
	try:
		config_parser.read(path)
	except Exception, err:
		app.logger.error(err)
		pass

	section = project["name"]
	if not config_parser.has_section(section):
		config_parser.add_section(section)

	for saved_key, config_key in {
		"username": "username",
		"local": "password"}.iteritems():
		config_value = project[config_key]
		config_parser.set(section, saved_key, config_value)
	app.logger.info("write_password_file")
	with open(path, "wb") as pw_file:
		config_parser.write(pw_file)


def read_password_file(project):
	try:
		config_parser = ConfigParser.ConfigParser()
		config_parser.read(os.path.abspath(os.path.join(project["path"], "..", "pw")))
		section = project["name"]
		if config_parser.has_section(section):
			for saved_key, config_key in {
				"username": "username",
				"local": "password"}.iteritems():
				if config_parser.has_option(section, saved_key):
					project[config_key] = config_parser.get(section, saved_key)
	except Exception, err:
		app.logger.exception(err)
		pass


def load_frontend_settings():
	config_parser = ConfigParser.ConfigParser()
	try:
		config_parser.read(os.path.expanduser("~/.vpc_frontend_settings.conf"))
	except IOError:
		pass
	if not config_parser.has_section("frontend"):
		config_parser.add_section("frontend")

	for key, default_value in NETWORKING_SETTINGS:
		if config_parser.has_option("frontend", key):
			app.config[key] = config_parser.getint("frontend", key)
		else:
			app.config[key] = default_value

	for key, default_value in DIRECTORY_SETTINGS:
		if config_parser.has_option("frontend", key):
			app.config[key] = config_parser.get("frontend", key)
		else:
			app.config[key] = default_value

	for key in STREAM_SETTINGS:
		if config_parser.has_option("frontend", key):
			app.config[key] = config_parser.getboolean("frontend", key)
		else:
			app.config[key] = True


def save_frontend_settings():
	path = os.path.expanduser("~/.vpc_frontend_settings.conf")

	config_parser = ConfigParser.ConfigParser()
	try:
		config_parser.read(path)
	except IOError, err:
		app.logger.exception(err)
		pass
	if not config_parser.has_section("frontend"):
		config_parser.add_section("frontend")
	config_parser.set("frontend", "VIUR_PROJECT_DIRECTORY", app.config["VIUR_PROJECT_DIRECTORY"])
	config_parser.set("frontend", "GOOGLE_SDK_PATH", app.config["GOOGLE_SDK_PATH"])
	config_parser.set("frontend", "FRONTEND_SERVER_PORT", app.config["FRONTEND_SERVER_PORT"])
	config_parser.set("frontend", "FRONTEND_ADMIN_PORT", app.config["FRONTEND_ADMIN_PORT"])
	config_parser.set("frontend", "REAL_SERVER_PORT", app.config["REAL_SERVER_PORT"])
	config_parser.set("frontend", "REAL_ADMIN_PORT", app.config["REAL_ADMIN_PORT"])
	config_parser.set("frontend", "LOG_CLOSE_ON_END_BUILD_VI", app.config["LOG_CLOSE_ON_END_BUILD_VI"])
	config_parser.set("frontend", "LOG_CLOSE_ON_END_DEPLOY", app.config["LOG_CLOSE_ON_END_DEPLOY"])
	config_parser.set("frontend", "LOG_CLOSE_ON_END_PROJECT", app.config["LOG_CLOSE_ON_END_PROJECT"])
	config_parser.write(open(path, "w"))


def main():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('-d', '--debug', action="store_true")
	args = parser.parse_args(sys.argv[1:])

	app.config["VIUR_PROJECT_DIRECTORY"] = "/Projects"
	app.config["GOOGLE_SDK_PATH"] = "/src/google_appengine"

	try:
		app.config.from_envvar("VPC_CONFIG")
	except:
		pass
	try:
		load_frontend_settings()
	except IOError:
		pass
	if not args.debug:
		http_server = WSGIServer(('', 5000), app)
		http_server.serve_forever()
	else:
		app.run(debug=True)


if __name__ == '__main__':
	main()
