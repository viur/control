# Viur Control #

A management web ui to control viur development instances.

![viur_control.png](https://bitbucket.org/repo/BKbGBb/images/3956103585-viur_control.png)

## Key Features ##
 * Viur project overview
 * starts/stops a viur dev instance
 * builds vi for your viur projects
 * you can deploy your instances
 * see realtime logs
 * feels well in docker containers (that was the main reason to write this software)

## dependencies/needed libs ##

* flask
* gevent
* yaml

## optionally install it


```
#!bash

cd control
sudo python setup.py install
```

## run it ##


```
#!bash

./viur_control.py
```


...or if you have installed it


```
#!bash

viur_control
```


now fire up your chrome brower with http://localhost:5000 for stock configuration

## configuration files

### frontend settings ###

Here we will store our prefered ports, which directory we are searching for viur projects and if we want to keep log windows open or close it when the tasks has finished. You can find the file in the your current user home directory at ~/.vpc_frontend_settings.conf

**OLD INFORMATION - config section needs to be rewritten**

an example

```
#!

[frontend]
viur_project_directory = /home/me/IdeaProjects
google_sdk_path = /home/me/dev/google_appengine
frontend_server_port = 10000
frontend_admin_port = 10001
log_close_on_end_build_vi = True
log_close_on_end_project = False
log_close_on_end_deploy = True
```

### project specific 'pw' file ###

When you start a new project, vpc will parse the dev server output and stores the credentials in the file 'pw' inside the project root directory - not the appengine directory. Existing pw files will be amended not completely overwritten. So it is possible to loose your crendentials for the current app id if the file already exists and the app id section is present in the pw file.

An example

```
#!

[app_id]
username = blahfoo@app_id.appspot.com
local = my local password
remote = the password of the deployed instance (security risk!!!)

[alternate_app_id]
username = blahfoo@alternate_app_id.appspot.com
local = my local password
remote = the password of the deployed instance (security risk!!!)
```