#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='viur_control',
    version='0.3',
    packages=find_packages(),
    zip_safe=False,
    install_requires=["flask", "gevent", "pyyaml"],
    include_package_data=True,
    package_data={"": ["templates/*", "static/css/*", "static/img/*", "static/javascript/*"]},
    entry_points="""
    [console_scripts]
    viur_control = viur_control.viur_control:main
    """,
    author="Stefan Kögl",
    author_email="info@mausbrand.de",
    description="viur control",
    long_description="",
    license="GPL v3",
    keywords="",
    url="https://bitbucket.org/viur/control"
)
